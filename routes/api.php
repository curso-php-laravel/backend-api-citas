<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function(){ 
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/medicos/document/{medIdentificacion}','MedicosController@getMedicoByDocument');
    Route::get('/medicos/nombres/{medNombres}','MedicosController@getMedicoByNombre');

    Route::apiResource('/medicos','MedicosController');
    Route::apiResource('/medicos','MedicosController');
    Route::apiResource('/pacientes','PacienteController');
    Route::apiResource('/tratamientos','TratamientoController');
});