<?php

use Illuminate\Database\Seeder;
use App\Medico;
class MedicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Medico::truncate(); //Limpia la tabla 

        //Se crearán dos registros de prueba en la tabla medicos

        Medico::create([
            'medIdentificacion' => '223344',
            'medNombres' => 'Juan Camilo',
            'medApellidos' => 'Mendoza'
        ]);

        Medico::create([
            'medIdentificacion' => '334455',
            'medNombres' => 'Maria Alejandra',
            'medApellidos' => 'Arias Rengifo'
        ]);
    }
}
