<?php

use Illuminate\Database\Seeder;
use App\Paciente;
class PacientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paciente::truncate();

        Paciente::create([
            'pacIdentificacion' => '334455',
            'pacNombres' => 'Maria Camila',
            'pacApellidos' => 'Alvear',
            'pacFechaNacimiento' => '1985-05-20',
            'pacSexo' => 'F'
        ]);

        Paciente::create([
            'pacIdentificacion' => '887766',
            'pacNombres' => 'Carlos Andres',
            'pacApellidos' => 'Sanchez',
            'pacFechaNacimiento' => '1992-05-20',
            'pacSexo' => 'M'
        ]);

    }
}
