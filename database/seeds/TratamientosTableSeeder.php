<?php

use Illuminate\Database\Seeder;
use App\Tratamiento;
class TratamientosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tratamiento::truncate();  //Elimina los datos existentes

        Tratamiento::create([
            'traDescripcion' => 'Tratamiento General',
            'traFechaInicio' => '2019-05-20',
            'traFechaFin' => '2019-06-10',
            'paciente_id' => '1'
        ]);

        Tratamiento::create([
            'traDescripcion' => 'Tratamiento Dos',
            'traFechaInicio' => '2019-05-10',
            'traFechaFin' => '2019-06-10',
            'paciente_id' => '2'
        ]);
    }
}
