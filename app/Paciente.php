<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tratamiento;
class Paciente extends Model
{
    //
    protected $fillable = ['pacIdentificacion','pacNombres','pacApellidos','pacFechaNacimiento','pacSexo'];
    
    //Relations
    public function tratamiento(){
        return $this->hasMany(Tratamiento::class);
    }
}
