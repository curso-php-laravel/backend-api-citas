<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Paciente;
class Tratamiento extends Model
{
    //
    protected $fillable = ['traDescripcion','traFechaInicio','traFechaFin','paciente_id'];

    public function paciente(){
        return $this->belongsTo(Paciente::class);
    }
}
