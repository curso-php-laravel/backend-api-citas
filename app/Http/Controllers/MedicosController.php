<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medico;
class MedicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       return Medico::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return Medico::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $medico = Medico::find($id);
        return  $medico;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $medico = Medico::find($id);
        $medico->update($request->all());
        return $medico;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $medico = Medico::find($id);
        $medico->delete();
        return $medico;                  

    }
    
    /**
     * get medico by Document number
     * @param int $medIdentificacion
     * @return \Illuminate\Http\Response
     */
    public function getMedicoByDocument($medIdentificacion){
        return Medico::where('medIdentificacion',$medIdentificacion)->first();
    }

    /**
     * get medico by Document number
     * @param String $medNombres
     * @return \Illuminate\Http\Response
     */
    public function getMedicoByNombre($medNombres){
        //echo $medNombres;
        return Medico::where('medNombres',$medNombres)->orWhere('medNombres','like','%'.$medNombres.'%')->get();
    }
}
